This repo contains high level and low level bindings to [libfive](https://libfive.com/).

It requires an installation of libfive to be present to work.