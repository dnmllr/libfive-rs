use three::color::WHITE;
use three::Object;

use rand::prelude::*;
use rand::thread_rng;

pub mod scene;
pub mod shapes;
pub mod tree;

const RANGE: f32 = 20.0;

pub fn render_scene(scene: &scene::Scene) {
    let mut window = three::Window::new("csg for the people");
    if let Some(geometry) = scene.render_mesh() {
        let mesh = window.factory.mesh(
            geometry.clone(),
            three::material::Pbr {
                ..Default::default()
            },
        );
        window.scene.add(&mesh);
        let mesh = window
            .factory
            .mesh(geometry, three::material::Wireframe { color: WHITE });
        window.scene.add(&mesh);
        let camera = window.factory.perspective_camera(60.0, 1.0..200.0);
        let mut camera_controls = three::controls::Orbit::builder(&camera)
            .position([0.0, -3.0, 0.0])
            .target([0.0, 0.0, 0.0])
            .up([1.0, 0.0, 0.0])
            .build();

        let light = window.factory.directional_light(0xffffff, 0.5);
        light.look_at([15.0, 35.0, 35.0], [0.0, 0.0, 2.0], None);
        let mut light_controls = three::controls::Orbit::builder(&camera)
            .position([0.0, -3.0, 0.0])
            .target([0.0, 0.0, 0.0])
            .up([1.0, 0.0, 0.0])
            .build();

        let mut rng = thread_rng();
        let lights = [
            0x12622, 0x003b36, 0xece5f0, 0xe98a15, 0x59114d, 0xa9ffcb, 0xb6eea6, 0xc0b298,
            0xa4668b, 0xaa4586,
        ]
        .iter()
        .map(|&color| {
            window
                .factory
                .directional_light(color, rng.gen_range(0.0, 1.0))
        })
        .collect::<Vec<_>>();
        for light in &lights {
            let pos = [
                rng.gen_range(-RANGE, RANGE),
                rng.gen_range(-RANGE, RANGE),
                rng.gen_range(-RANGE, RANGE),
            ];
            light.look_at(pos, [0.0, 0.0, 0.0], None);
            window.scene.add(light);
        }

        while window.update() && !window.input.hit(three::KEY_ESCAPE) {
            if window.input.hit(three::KEY_SPACE) {
                for light in &lights {
                    let pos = [
                        rng.gen_range(-RANGE, RANGE),
                        rng.gen_range(-RANGE, RANGE),
                        rng.gen_range(-RANGE, RANGE),
                    ];
                    light.look_at([20.0, 35.0, 35.0], [0.0, 0.0, 0.0], None);
                    light.set_position(pos);
                }
            }
            camera_controls.update(&window.input);
            light_controls.update(&window.input);
            light.look_at([20.0, 35.0, 35.0], [0.0, 0.0, 0.0], None);
            window.render(&camera);
        }
    }
}
