use mint::Point3;
use std::ptr::NonNull;
use three::{Geometry, Shape};

use libfive_sys::{
    libfive_mesh_delete, libfive_region3, libfive_tree, libfive_tree_, libfive_tree_binary,
    libfive_tree_const, libfive_tree_delete, libfive_tree_remap, libfive_tree_render_mesh,
    libfive_tree_unary, libfive_tree_x, libfive_tree_y, libfive_tree_z,
};

use crate::tree::opcodes::Opcode;
use crate::tree::{Coord, Cos, Sin, Sqrt};

pub struct TreePtr {
    ptr: NonNull<libfive_tree_>,
}

impl Drop for TreePtr {
    fn drop(&mut self) {
        unsafe { libfive_tree_delete(self.ptr.as_ptr()) }
    }
}

impl AsRef<TreePtr> for TreePtr {
    fn as_ref(&self) -> &TreePtr {
        self
    }
}

impl From<libfive_tree> for TreePtr {
    fn from(t: libfive_tree) -> Self {
        Self {
            ptr: NonNull::new(t).expect("libfive failed to allocate tree_2"),
        }
    }
}

impl TreePtr {
    pub fn x() -> Self {
        unsafe { libfive_tree_x() }.into()
    }

    pub fn y() -> Self {
        unsafe { libfive_tree_y() }.into()
    }

    pub fn z() -> Self {
        unsafe { libfive_tree_z() }.into()
    }

    pub fn constant(f: f32) -> Self {
        unsafe { libfive_tree_const(f) }.into()
    }

    pub fn unary(op: Opcode, a: &Self) -> Self {
        Self::from(unsafe { libfive_tree_unary(op.lookup_code(), a.ptr.as_ptr()) })
    }

    pub fn binary(op: Opcode, a: &Self, b: &Self) -> Self {
        Self::from(unsafe { libfive_tree_binary(op.lookup_code(), a.ptr.as_ptr(), b.ptr.as_ptr()) })
    }

    pub fn compare<T: Into<Self>>(&self, b: T) -> Self {
        Self::binary(Opcode::Compare, self, &b.into())
    }

    pub fn extrude_z<T: Into<Self>>(&self, z: T) -> Self {
        self.max((Coord::Z - z).max(0.0 - Coord::Z))
    }

    pub fn extrude_x<T: Into<Self>>(&self, x: T) -> Self {
        self.max((Coord::X - x).max(0.0 - Coord::X))
    }

    pub fn extrude_y<T: Into<Self>>(&self, y: T) -> Self {
        self.max((Coord::Y - y).max(0.0 - Coord::Y))
    }

    pub fn max<T: Into<Self>>(&self, b: T) -> Self {
        Self::binary(Opcode::Max, self, &b.into())
    }

    pub fn min<T: Into<Self>>(&self, b: T) -> Self {
        Self::binary(Opcode::Min, self, &b.into())
    }

    pub fn remap(&self, x: &Self, y: &Self, z: &Self) -> Self {
        Self::from(unsafe {
            libfive_tree_remap(
                self.ptr.as_ptr(),
                x.ptr.as_ptr(),
                y.ptr.as_ptr(),
                z.ptr.as_ptr(),
            )
        })
    }

    pub fn render(&self, region: libfive_region3, resolution: f32) -> Geometry {
        let mesh = unsafe { libfive_tree_render_mesh(self.ptr.as_ptr(), region, resolution) };
        let vertices =
            unsafe { std::slice::from_raw_parts((*mesh).verts, (*mesh).vert_count as usize) }
                .into_iter()
                .map(|x| Point3 {
                    x: x.x,
                    y: x.y,
                    z: x.z,
                })
                .collect();
        let faces = unsafe { std::slice::from_raw_parts((*mesh).tris, (*mesh).tri_count as usize) }
            .into_iter()
            .map(|x| [x.a, x.b, x.c])
            .collect();
        unsafe {
            libfive_mesh_delete(mesh);
        }
        Geometry {
            faces,
            base: Shape {
                vertices,
                ..Shape::default()
            },
            ..Geometry::default()
        }
    }
}

impl Sin for TreePtr {
    type Output = Self;

    fn sin(self) -> Self::Output {
        Self::unary(Opcode::Sin, &self)
    }
}

impl Cos for TreePtr {
    type Output = Self;

    fn cos(self) -> Self::Output {
        Self::unary(Opcode::Cos, &self)
    }
}

impl Sin for &TreePtr {
    type Output = TreePtr;

    fn sin(self) -> Self::Output {
        TreePtr::unary(Opcode::Sin, self)
    }
}

impl Cos for &TreePtr {
    type Output = TreePtr;

    fn cos(self) -> Self::Output {
        TreePtr::unary(Opcode::Cos, self)
    }
}

impl Sqrt for TreePtr {
    type Output = TreePtr;

    fn sqrt(self) -> Self::Output {
        TreePtr::unary(Opcode::Sqrt, &self)
    }
}

impl Sqrt for &TreePtr {
    type Output = TreePtr;

    fn sqrt(self) -> Self::Output {
        TreePtr::unary(Opcode::Sqrt, self)
    }
}

impl std::ops::BitOr for TreePtr {
    type Output = TreePtr;

    fn bitor(self, rhs: Self) -> Self::Output {
        self.min(rhs)
    }
}

impl std::ops::BitAnd for TreePtr {
    type Output = TreePtr;

    fn bitand(self, rhs: Self) -> Self::Output {
        self.max(rhs)
    }
}

impl std::ops::Neg for TreePtr {
    type Output = TreePtr;

    fn neg(self) -> Self::Output {
        TreePtr::unary(Opcode::Neg, &self)
    }
}

impl std::ops::Neg for &TreePtr {
    type Output = TreePtr;

    fn neg(self) -> Self::Output {
        TreePtr::unary(Opcode::Neg, self)
    }
}

impl std::ops::BitXor for TreePtr {
    type Output = TreePtr;

    fn bitxor(self, rhs: Self) -> Self::Output {
        self & -rhs
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cmp() {}
}
