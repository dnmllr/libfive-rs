mod opcodes;
mod ptr;

use self::opcodes::Opcode;
pub use self::ptr::TreePtr;
use std::ops::*;

#[derive(Debug, Copy, Clone, PartialOrd, PartialEq, Eq, Ord)]
pub enum Coord {
    X,
    Y,
    Z,
}

impl From<&Coord> for TreePtr {
    fn from(r: &Coord) -> Self {
        match r {
            Coord::X => TreePtr::x(),
            Coord::Y => TreePtr::y(),
            Coord::Z => TreePtr::z(),
        }
    }
}

impl From<Coord> for TreePtr {
    fn from(r: Coord) -> Self {
        match r {
            Coord::X => TreePtr::x(),
            Coord::Y => TreePtr::y(),
            Coord::Z => TreePtr::z(),
        }
    }
}

impl From<f32> for TreePtr {
    fn from(f: f32) -> Self {
        TreePtr::constant(f)
    }
}

impl From<i32> for TreePtr {
    fn from(i: i32) -> Self {
        TreePtr::constant(i as f32)
    }
}

pub trait Powf<Rhs = Self> {
    type Output;
    fn powf(self, rhs: Rhs) -> Self::Output;
}

pub trait Sqrt {
    type Output;
    fn sqrt(self) -> Self::Output;
}

pub trait Sin {
    type Output;
    fn sin(self) -> Self::Output;
}

pub trait Cos {
    type Output;
    fn cos(self) -> Self::Output;
}

impl Sin for Coord {
    type Output = TreePtr;

    fn sin(self) -> Self::Output {
        let t: TreePtr = self.into();
        t.sin()
    }
}

impl Cos for Coord {
    type Output = TreePtr;

    fn cos(self) -> Self::Output {
        let t: TreePtr = self.into();
        t.cos()
    }
}

impl Sqrt for Coord {
    type Output = TreePtr;

    fn sqrt(self) -> Self::Output {
        let t: TreePtr = self.into();
        t.sqrt()
    }
}

macro_rules! impl_bin {
    ($op:tt, $m:ident, $c:path) => {
        impl<R: Into<TreePtr>> $op<R> for Coord {
            type Output = TreePtr;

            fn $m(self, rhs: R) -> Self::Output {
                TreePtr::binary($c, &self.into(), &rhs.into())
            }
        }

        impl<R: Into<TreePtr>> $op<R> for &Coord {
            type Output = TreePtr;

            fn $m(self, rhs: R) -> Self::Output {
                TreePtr::binary($c, &self.into(), &rhs.into())
            }
        }

        impl<R: Into<TreePtr>> $op<R> for TreePtr {
            type Output = TreePtr;

            fn $m(self, rhs: R) -> Self::Output {
                TreePtr::binary($c, &self, &rhs.into())
            }
        }

        impl<R: Into<TreePtr>> $op<R> for &TreePtr {
            type Output = TreePtr;

            fn $m(self, rhs: R) -> Self::Output {
                TreePtr::binary($c, self, &rhs.into())
            }
        }

        impl $op<&TreePtr> for TreePtr {
            type Output = TreePtr;

            fn $m(self, rhs: &TreePtr) -> Self::Output {
                TreePtr::binary($c, &self, rhs)
            }
        }

        impl $op<&TreePtr> for &TreePtr {
            type Output = TreePtr;

            fn $m(self, rhs: &TreePtr) -> Self::Output {
                TreePtr::binary($c, self, rhs)
            }
        }

        impl $op<&TreePtr> for f32 {
            type Output = TreePtr;

            fn $m(self, rhs: &TreePtr) -> Self::Output {
                TreePtr::binary($c, &self.into(), rhs)
            }
        }

        impl $op<TreePtr> for f32 {
            type Output = TreePtr;

            fn $m(self, rhs: TreePtr) -> Self::Output {
                TreePtr::binary($c, &self.into(), &rhs)
            }
        }

        impl $op<&Coord> for f32 {
            type Output = TreePtr;

            fn $m(self, rhs: &Coord) -> Self::Output {
                TreePtr::binary($c, &self.into(), &rhs.into())
            }
        }

        impl $op<Coord> for f32 {
            type Output = TreePtr;

            fn $m(self, rhs: Coord) -> Self::Output {
                TreePtr::binary($c, &self.into(), &rhs.into())
            }
        }
    };
}

impl_bin!(Add, add, Opcode::Add);
impl_bin!(Sub, sub, Opcode::Sub);
impl_bin!(Mul, mul, Opcode::Mul);
impl_bin!(Div, div, Opcode::Div);
impl_bin!(Powf, powf, Opcode::Pow);
