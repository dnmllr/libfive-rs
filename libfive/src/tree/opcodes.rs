use std::collections::HashMap;
use std::ffi::CString;
use std::fmt::{Display, Error, Formatter};
use std::os::raw::c_int;
use std::sync::RwLock;

use lazy_static::lazy_static;

use libfive_sys::libfive_opcode_enum;

#[derive(Eq, PartialEq, Copy, Clone, Debug, Hash)]
pub enum Opcode {
    Add,
    Sub,
    Mul,
    Not,
    Neg,
    Abs,
    Sqrt,
    Pow,
    Max,
    Min,
    Sin,
    Cos,
    Div,
    Compare,
}

impl Display for Opcode {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "Opcode::{:?}", self)
    }
}

lazy_static! {
    static ref OPCODE_BINDING_LOOKUP: RwLock<HashMap<Opcode, c_int>> = RwLock::new(HashMap::new());
}

impl Into<usize> for Opcode {
    fn into(self) -> usize {
        self as usize
    }
}

impl Into<CString> for Opcode {
    fn into(self) -> CString {
        match self {
            Opcode::Add => CString::new("add"),
            Opcode::Sub => CString::new("sub"),
            Opcode::Mul => CString::new("mul"),
            Opcode::Neg => CString::new("neg"),
            Opcode::Abs => CString::new("abs"),
            Opcode::Not => CString::new("recip"),
            Opcode::Sqrt => CString::new("sqrt"),
            Opcode::Pow => CString::new("pow"),
            Opcode::Max => CString::new("max"),
            Opcode::Min => CString::new("min"),
            Opcode::Div => CString::new("div"),
            Opcode::Sin => CString::new("sin"),
            Opcode::Cos => CString::new("cos"),
            Opcode::Compare => CString::new("compare"),
        }
        .expect("static strings should be convertable to CStrings")
    }
}

impl From<&Opcode> for usize {
    fn from(opcode: &Opcode) -> Self {
        *opcode as usize
    }
}

impl Opcode {
    fn as_cstring(self) -> CString {
        self.into()
    }

    pub fn lookup_code(self) -> c_int {
        let read = OPCODE_BINDING_LOOKUP.read().unwrap();
        if let Some(v) = read.get(&self) {
            *v
        } else {
            drop(read);
            let res = unsafe { libfive_opcode_enum(self.as_cstring().as_ptr()) };
            OPCODE_BINDING_LOOKUP.write().unwrap().insert(self, res);
            res
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_values() {
        assert_eq!(17, Opcode::Add.lookup_code(), "add code should be 17");

        assert_eq!(21, Opcode::Sub.lookup_code(), "sub code should be 21");

        assert_eq!(18, Opcode::Mul.lookup_code(), "sub code should be 18");
    }

    #[test]
    fn test_display() {
        assert_eq!(
            "Opcode::Pow",
            format!("{}", Opcode::Pow),
            "display should format opcodes appropriately"
        );
    }
}
