use libfive_sys::{libfive_interval, libfive_region3};
use three::Geometry;

use crate::tree::TreePtr;
use std::ops::AddAssign;

pub struct Scene {
    region: libfive_region3,
    resolution: f32,
    ptr: Option<TreePtr>,
}

impl Scene {
    pub fn new() -> Scene {
        Default::default()
    }

    pub fn set_x(&mut self, from: f32, to: f32) -> &mut Self {
        self.region.X.lower = from;
        self.region.X.upper = to;
        self
    }

    pub fn set_y(&mut self, from: f32, to: f32) -> &mut Self {
        self.region.Y.lower = from;
        self.region.Y.upper = to;
        self
    }

    pub fn set_z(&mut self, from: f32, to: f32) -> &mut Self {
        self.region.Z.lower = from;
        self.region.Z.upper = to;
        self
    }

    pub fn set_resolution(&mut self, resolution: f32) -> &mut Self {
        self.resolution = resolution;
        self
    }

    pub fn render_mesh(&self) -> Option<Geometry> {
        self.ptr
            .as_ref()
            .map(|t| t.render(self.region, self.resolution))
    }
}

impl<T: Into<TreePtr>> AddAssign<T> for Scene {
    fn add_assign(&mut self, rhs: T) {
        if let Some(ptr) = self.ptr.take() {
            self.ptr = Some(rhs.into() | ptr)
        } else {
            self.ptr = Some(rhs.into())
        }
    }
}

impl Default for Scene {
    fn default() -> Self {
        Scene {
            region: libfive_region3 {
                X: libfive_interval {
                    lower: -10.0,
                    upper: 10.0,
                },
                Y: libfive_interval {
                    lower: -10.0,
                    upper: 10.0,
                },
                Z: libfive_interval {
                    lower: -10.0,
                    upper: 10.0,
                },
            },
            resolution: 10.0,
            ptr: None,
        }
    }
}
