use crate::tree::{Coord, Cos, Powf, Sin, Sqrt, TreePtr};
use std::f32::consts::PI;

pub trait Twist {
    fn twist(&self, amount: f32) -> TreePtr;
}

impl Twist for TreePtr {
    fn twist(&self, amount: f32) -> TreePtr {
        let x = (Coord::Z * amount).cos() * Coord::X + (Coord::Z * amount).sin() * Coord::Y;
        let y = (Coord::Z * amount).cos() * Coord::Y - (Coord::Z * amount).sin() * Coord::X;
        self.remap(&x, &y, &Coord::Z.into())
    }
}

pub trait Translate {
    fn translate<X, Y, Z>(&self, x: X, y: Y, z: Z) -> TreePtr
    where
        X: Into<TreePtr>,
        Y: Into<TreePtr>,
        Z: Into<TreePtr>;

    fn translate_x<T: Into<TreePtr>>(&self, x: T) -> TreePtr {
        self.translate(x, 0.0, 0.0)
    }

    fn translate_y<T: Into<TreePtr>>(&self, y: T) -> TreePtr {
        self.translate(0.0, y, 0.0)
    }

    fn translate_z<T: Into<TreePtr>>(&self, z: T) -> TreePtr {
        self.translate(0.0, 0.0, z)
    }
}

#[inline(always)]
fn translate<X, Y, Z>(ptr: &TreePtr, x: X, y: Y, z: Z) -> TreePtr
where
    X: Into<TreePtr>,
    Y: Into<TreePtr>,
    Z: Into<TreePtr>,
{
    ptr.remap(
        &(Coord::X - x.into()),
        &(Coord::Y - y.into()),
        &(Coord::Z - z.into()),
    )
}

impl<T: AsRef<TreePtr>> Translate for T {
    fn translate<X, Y, Z>(&self, x: X, y: Y, z: Z) -> TreePtr
    where
        X: Into<TreePtr>,
        Y: Into<TreePtr>,
        Z: Into<TreePtr>,
    {
        translate(self.as_ref(), x, y, z)
    }
}

pub trait Rotate {
    fn rotate_x(&self, r: f32) -> TreePtr;
    fn rotate_y(&self, r: f32) -> TreePtr;
    fn rotate_z(&self, r: f32) -> TreePtr;
}

impl Rotate for TreePtr {
    fn rotate_x(&self, r: f32) -> TreePtr {
        let cos_angle = (r * PI * 2.0).cos();
        let sin_angle = (r * PI * 2.0).sin();
        self.remap(
            &Coord::X.into(),
            &(cos_angle * Coord::Y + sin_angle * Coord::Z),
            &(-sin_angle * Coord::Y + cos_angle * Coord::Z),
        )
    }

    fn rotate_y(&self, r: f32) -> TreePtr {
        let cos_angle = (r * PI * 2.0).cos();
        let sin_angle = (r * PI * 2.0).sin();
        self.remap(
            &(cos_angle * Coord::X + sin_angle * Coord::Z),
            &Coord::Y.into(),
            &(-sin_angle * Coord::X + cos_angle * Coord::Z),
        )
    }

    fn rotate_z(&self, r: f32) -> TreePtr {
        let cos_angle = (r * PI * 2.0).cos();
        let sin_angle = (r * PI * 2.0).sin();
        self.remap(
            &(cos_angle * Coord::X + sin_angle * Coord::Y),
            &(-sin_angle * Coord::X + cos_angle * Coord::Y),
            &Coord::Z.into(),
        )
    }
}

pub fn circle<R: Into<TreePtr>>(radius: R) -> TreePtr {
    (Coord::X.powf(2.0) + Coord::Y.powf(2.0)).sqrt() - radius
}

pub fn sphere<R: Into<TreePtr>>(radius: R) -> TreePtr {
    (Coord::X.powf(2.0) + Coord::Y.powf(2.0) + Coord::Z.powf(2.0)).sqrt() - radius.into()
}

pub fn square(side: f32) -> TreePtr {
    rectangle(side, side)
}

pub fn rectangle(width: f32, height: f32) -> TreePtr {
    (-width / 2.0 - Coord::X)
        .max(Coord::X - width / 2.0)
        .max(-height / 2.0 - Coord::Y)
        .max(Coord::Y - height / 2.0)
}

#[cfg(test)]
mod tests {
    use crate::shapes::{circle, Translate};
    use crate::tree::Coord;

    #[test]
    fn test_circle() {
        let _ = circle(Coord::X + 0.32).translate(0.1, 0.2, 0.3);
    }
}
