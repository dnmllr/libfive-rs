extern crate libfive;

use libfive::scene::Scene;
use libfive::shapes;
use libfive::shapes::{Translate, Twist};

fn main() {
    // set up the scene
    let mut scene = Scene::new();

    scene.set_resolution(100.0);

    // CSG!!!
    let shape =
        shapes::square(2.0).extrude_z(7.0).twist(0.5) ^ shapes::sphere(2.0).translate_z(7.0);

    scene += (shape
        ^ shapes::square(3.0)
            .extrude_z(1.5)
            .translate_z(3.0)
            .translate_x(1.5))
        | shapes::square(1.0)
            .extrude_z(0.75)
            .translate_z(3.0)
            .translate_x(0.5)
            .translate_z(-3.5);

    // render it
    libfive::render_scene(&scene);
}
