extern crate libfive;

use libfive::scene::Scene;
use libfive::shapes;

fn main() {
    // set up the scene
    let mut scene = Scene::new();

    // CSG!!!
    let shape = shapes::rectangle(0.2, 0.3).extrude_z(0.4);

    scene += shape;

    // render it
    libfive::render_scene(&scene);
}
