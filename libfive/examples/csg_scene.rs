extern crate libfive;

use libfive::scene::Scene;
use libfive::shapes;
use libfive::shapes::Translate;

fn main() {
    // set up the scene
    let mut scene = Scene::new();

    scene.set_resolution(50.0);

    // CSG!!!
    let shape = (shapes::sphere(1.0).translate_x(1.0) | shapes::sphere(1.0))
        ^ shapes::sphere(1.0).translate_y(0.5).translate_z(0.5);

    scene += shape;

    // render it
    libfive::render_scene(&scene);
}
