extern crate libfive;

use libfive::scene::Scene;
use libfive::shapes;
use libfive::shapes::{Rotate, Translate};

fn main() {
    // set up the scene
    let mut scene = Scene::new();

    let width = 2.0;
    let height = 0.7;
    let e = 0.3;

    let mut shape = shapes::rectangle(width, height).extrude_z(e);

    for i in 1..6 {
        shape = shape
            | shapes::rectangle(width, height)
                .extrude_z(i as f32 * e)
                .translate_y(i as f32 * height);
    }

    shape = shape
        ^ (shapes::square(1.2)
            .rotate_z(0.125)
            .translate_y(3.0 * height));

    scene += shape;

    // render it
    libfive::render_scene(&scene);
}
